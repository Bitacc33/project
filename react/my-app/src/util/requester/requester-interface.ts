interface Requester {
    get(endpoint: string): Promise<any>,
    post(endpoint: string, data?: any): Promise<any>,
    delete(endpoint: string, data?: any): Promise<any>,
    patch(endpoint: string, data?: any): Promise<any>,
}

export default Requester;