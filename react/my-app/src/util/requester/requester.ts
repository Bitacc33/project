import axios, { AxiosRequestConfig, AxiosInstance } from "axios";

import Requester from "./requester-interface";

const buildRequestConfig = (method: "GET" | "POST" | "PATCH" | "DELETE", endpoint: string, data?: any): AxiosRequestConfig => {
    const headers: any = {
        "Content-Type": "application/json"
    };

    return { headers, method, url: endpoint, data };
};

const prepareRequest = (method: "GET" | "POST" | "PATCH" | "DELETE", axios: AxiosInstance) => {
    return (endpoint: string, data?: any) => {
        const requestConfig = buildRequestConfig(method, endpoint, data);

        return axios.request(requestConfig);
    }
}

const initRequester = (baseURL: string): Requester => {
    const instance = axios.create({ baseURL, withCredentials: true });

    return {
        get: prepareRequest("GET", instance),
        post: prepareRequest("POST", instance),
        patch: prepareRequest("PATCH", instance),
        delete: prepareRequest("DELETE", instance),
    };
}

export default initRequester;