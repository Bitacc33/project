import RegistrationLocale from "../../../../dispensers/forms/registration/registration-interface";

const en_USRegistration: RegistrationLocale = {
    formHeader: "Registration",
    emailLabel: "Email",
    usernameLabel: "Username",
    passwordLabel: "Password",
    confirmPasswordLabel: "Confirm Password",
    submitBtn: "Sign Up",
    hasAnAccount: "Have an acoount? Log in",
    last_nameLabel: "Last name"
};

export default en_USRegistration;