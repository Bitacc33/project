import FooterLocales from "../../../../dispensers/navigation/footer-interface";

const en_USFooter: FooterLocales = {
    about: "ABOUT",
    contacts: "CONTACTS",
    trademark: "E-SHOP, Inc. ",
    rights: "All rights reserved. All trademarks referenced herein are the properties of their respective owners.",
    careers: "CAREERS"
};

export default en_USFooter;