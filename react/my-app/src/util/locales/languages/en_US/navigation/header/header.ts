import HeaderTranslates from "../../../../dispensers/navigation/header-interface";

const en_USHeader: HeaderTranslates = {
    signUpBtn: "Sign up",
    loginBtn: "Log in",
    about: "About",
    bestSellers: "Best Sellers",
};

export default en_USHeader;