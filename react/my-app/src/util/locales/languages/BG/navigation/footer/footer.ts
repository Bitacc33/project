import FooterLocales from "../../../../dispensers/navigation/footer-interface";

const bulgarianFooter: FooterLocales = {
    about: "ЗА НАС", 
    contacts: "КОНТАКТИ",
    trademark: "E-Shop, Inc. Всички права запазени.",
    rights: " Всички търговски марки опоменати са собственост на техните собственици.",
    careers: "КАРИЕРИ"
};

export default bulgarianFooter;