import HeaderTranslates from "../../../../dispensers/navigation/header-interface";

const bulgarianHeader: HeaderTranslates = {
    signUpBtn: "Регистрация",
    loginBtn: "Вход",
    about: "За нас",
    bestSellers: "Най-продавани",
}

export default bulgarianHeader;