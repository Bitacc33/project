import RegistrationLocale from "../../../../dispensers/forms/registration/registration-interface";

const bulgarianRegistration: RegistrationLocale = {
    formHeader: "Регистрация",
    emailLabel: "Емайл",
    usernameLabel: "Потребителско име",
    passwordLabel: "Парола",
    confirmPasswordLabel: "Потвърди парола",
    submitBtn: "Изпрати",
    hasAnAccount: "Имате акаунт? Вход",
    last_nameLabel: "Фамилно име*"
}

export default bulgarianRegistration;