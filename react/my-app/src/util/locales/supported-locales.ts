export const supportedLocales = {
    en_US: "en_US",
    BG: "BG",
};

export const languageLiterals = {
    en_US: "English (United States)",
    BG: "Български",
}