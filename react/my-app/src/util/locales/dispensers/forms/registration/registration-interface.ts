interface RegistrationLocale {
    formHeader: string,
    emailLabel: string,
    usernameLabel: string,
    passwordLabel: string,
    confirmPasswordLabel: string,
    submitBtn: string,
    hasAnAccount: string,
    last_nameLabel: string,
}

export default RegistrationLocale;