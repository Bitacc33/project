import bulgarianRegistration from "../../../languages/BG/forms/registration/registration";
import en_USRegistration from "../../../languages/en_US/forms/registration/registration";

import RegistrationLocale from "./registration-interface";

const translates = {
    BG: bulgarianRegistration,
    en_US: en_USRegistration,
}

const registerDispenser = (locale: string): RegistrationLocale => {
    return translates[locale]
        ? translates[locale]
        : translates.en_US;
};

export default registerDispenser;