import Footer from "../../../../components/navigation/Footer/Footer";

interface FooterLocales {
    careers: string,
    about: string,
    contacts: string,
    trademark: string,
    rights: string,
}

export default FooterLocales;