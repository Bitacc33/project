interface HeaderTranslates {
    about: string,
    bestSellers: string,
    loginBtn: string,
    signUpBtn: string,
}

export default HeaderTranslates;