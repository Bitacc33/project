import bulgarianHeader from "../../languages/BG/navigation/header/header";
import en_USHeader from "../../languages/en_US/navigation/header/header";

import bulgarianFooter from "../../languages/BG/navigation/footer/footer";
import en_USFooter from "../../languages/en_US/navigation/footer/footer";

import HeaderTranslates from "./header-interface";
import FooterLocales from "./footer-interface";

const headerTranslates = {
    BG: bulgarianHeader,
    en_US: en_USHeader,
};

export const headerDispenser = (locale: string): HeaderTranslates => {
    return headerTranslates[locale]
        ? headerTranslates[locale]
        : headerTranslates.en_US;
};

const footerTranslates = {
    BG: bulgarianFooter,
    en_US: en_USFooter,
}

export const footerDispenser = (locale: string): FooterLocales => {
    return footerTranslates[locale]
        ? footerTranslates[locale]
        : footerTranslates.en_US;
};