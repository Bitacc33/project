const socialMediaLinks = {
    twitter: "http://twitter.com",
    facebook: "http://facebook.com",
    instagram: "http://instagram.com",
}

export default socialMediaLinks;