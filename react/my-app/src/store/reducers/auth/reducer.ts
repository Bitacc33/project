import { AuthReducer, AuthActions, LoginFailAction, LoginSuccessAction } from "./interfaces";
import { actionTypes } from "./actions";

const initialState = { user: null, error: null, isLoading: false, success: false, isAuthenticating: false } as AuthReducer;

const loginError = (state: AuthReducer, action: AuthActions) => {
    return {
        ...state,
        error: action.payload,
        isLoading: false,
    };
};

const loginSuccess = (state: AuthReducer, action: AuthActions) => {
    return {
        ...state,
        error: null,
        isLoading: false,
        success: true,
        user: action.payload,
        isAuthenticating: false,
    };
};

const loginStart = (state: AuthReducer): AuthReducer => {
    return {
        ...state,
        error: null,
        isLoading: true,
    };
};

const firstTimeLoadFail = (state: AuthReducer): AuthReducer => {
    return {
        ...state,
        isAuthenticating: false,
    };
};

const firstTimeLoadStart = (state: AuthReducer): AuthReducer => {
    return {
        ...state,
        isAuthenticating: true,
    };
};

const logoutSuccess = (state: AuthReducer): AuthReducer => {
    return initialState;
};

const logoutFail = (state: AuthReducer): AuthReducer => {
    return {
        ...state,
        isAuthenticating: false,
    };
};

const authReducer = (state: AuthReducer = initialState, action: AuthActions) => {
    switch (action.type) {
        case actionTypes.LOGIN_START:
            return loginStart(state);
        case actionTypes.LOGIN_SUCCESS:
            return loginSuccess(state, action);
        case actionTypes.LOGIN_ERROR:
            return loginError(state, action);
        case actionTypes.LOAD_AUTH_FAIL:
            return firstTimeLoadFail(state);
        case actionTypes.LOAD_AUTH_START:
            return firstTimeLoadStart(state);
        case actionTypes.LOGOUT_SUCCESS:
            return logoutSuccess(state);
        case actionTypes.LOGOUT_FAIL:
            return logoutFail(state);
        default:
            return state;
    }
};

export default authReducer;