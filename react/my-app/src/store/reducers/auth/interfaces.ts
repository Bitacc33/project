import { actionTypes } from "./actions";
import { Credentials, LoggedUser } from "../../../services/user-service/interfaces";

export interface AuthReducer {
    error: string | null | undefined,
    user: LoggedUser | null | undefined,
    success: boolean,
    isLoading: boolean,
    isAuthenticating: boolean,
}

export interface LoginStartAction {
    type: typeof actionTypes.LOGIN_START,
    payload: string,
}

export interface LoginSuccessAction {
    type: typeof actionTypes.LOGIN_SUCCESS,
    payload: LoggedUser
}

export interface LoginFailAction {
    type: typeof actionTypes.LOGIN_ERROR,
    payload: string,
}

export type AuthActions =
    | LoginStartAction
    | LoginSuccessAction
    | LoginFailAction;