import { createSelector } from "reselect";
import { LoggedUser } from "../../../services/user-service/interfaces";
import SystemState from "../../system-state";

const userSelector = (state: SystemState): LoggedUser | null | undefined => {
    return state.auth.user;
};

const errorSelector = (state: SystemState): string | null | undefined => {
    return state.auth.error;
};

const isLoadingSelector = (state: SystemState): boolean => {
    return state.auth.isLoading;
};

const isAuthenticatingSelector = (state: SystemState): boolean => {
    return state.auth.isAuthenticating;
};

const successSelector = (state: SystemState): boolean => {
    return state.auth.success;
};

export const loginSelector = createSelector([errorSelector, isLoadingSelector, successSelector],
    (error, isLoading, success) => {
        return {
            error,
            isLoading,
            success,
        };
    })