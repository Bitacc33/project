import { Credentials, LoggedUser } from "../../../services/user-service/interfaces";
import { LoginSuccessAction, LoginFailAction } from "./interfaces";

import userService from "../../../services/user-service/user-service";

export const actionTypes = {
    LOGIN_START: "[AUTH]LOGIN_START",
    LOGIN_SUCCESS: "[AUTH]LOGIN_SUCCESS",
    LOGIN_ERROR: "[AUTH]LOGIN_ERROR",
    LOAD_AUTH_FAIL: "[AUTH]LOAD_FAIL",
    LOAD_AUTH_START: "[AUTH]LOAD_START",
    LOGOUT_SUCCESS: "[AUTH]LOGOUT_SUCCESS",
    LOGOUT_FAIL: "[AUTH]LOGOUT_FAIL"
};

const loginSuccess = (user: LoggedUser): LoginSuccessAction => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: user
    }
};

const loginFail = (): LoginFailAction => {
    return {
        type: actionTypes.LOGIN_ERROR,
        payload: "Wrong username or password"
    }
};

const setLoading = () => {
    return {
        type: actionTypes.LOGIN_START,
    };
};

const startLogin = (credentials: Credentials) => {
    return (dispatch: any) => {
        dispatch(setLoading());
        userService.login(credentials)
            .then(({ data }) => {
                dispatch(loginSuccess(data));
            }).catch(err => {
                dispatch(loginFail());
            });
    }
};

const authenticatingStart = () => {
    return {
        type: actionTypes.LOAD_AUTH_START
    };
}

const firstTimeLoadFail = () => {
    return {
        type: actionTypes.LOAD_AUTH_FAIL,
    };
};

const firstTimeLoadAuth = () => {
    return (dispatch: any) => {
        dispatch(authenticatingStart());

        userService.authenticate()
            .then(({ data }) => {
                dispatch(loginSuccess(data));
            })
            .catch(err => {
                dispatch(firstTimeLoadFail());
            });
    };
};

const logoutSuccess = () => {
    return {
        type: actionTypes.LOGOUT_SUCCESS
    };
}

const logoutFail = () => {
    return {
        type: actionTypes.LOGOUT_FAIL
    };
}

const logout = () => {
    return (dispatch: any) => {
        dispatch(authenticatingStart());

        userService.logout()
            .then(res => {
                dispatch(logoutSuccess());
            }).catch(err => {
                dispatch(logoutFail());
            });
    };
};

export const actionCreators = {
    startLogin,
    firstTimeLoadAuth,
    logout,
};