import { actionTypes } from "./actions";
import { supportedLocales } from "../../../util/locales/supported-locales";

export type ChangeLocalePayload =
    | typeof supportedLocales.BG
    | typeof supportedLocales.en_US;

export interface ChangeLocaleAction {
    type: typeof actionTypes.CHANGE_LOCALE,
    payload: ChangeLocalePayload
}

export type LocaleACtions = ChangeLocaleAction;

export interface LocaleReducer {
    lang: string,
}