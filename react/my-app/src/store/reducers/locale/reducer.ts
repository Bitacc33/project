import { ChangeLocaleAction, LocaleACtions, LocaleReducer } from "./locale-reducer-interface";
import { actionTypes } from "./actions";
import { supportedLocales } from "../../../util/locales/supported-locales";

const LOCAL_STORAGE_KEY = "LOCALE_KEY";

const changeLocale = (state: LocaleReducer, action: ChangeLocaleAction) => {
    localStorage.setItem(LOCAL_STORAGE_KEY, action.payload);
    return {
        ...state,
        lang: action.payload,
    };
};

const currentLocale = localStorage.getItem(LOCAL_STORAGE_KEY)
    ? localStorage.getItem(LOCAL_STORAGE_KEY)
    : supportedLocales.en_US;

const initialState = {
    lang: currentLocale!
};

const localeReducer = (state: LocaleReducer = initialState, action: LocaleACtions) => {
    switch (action.type) {
        case actionTypes.CHANGE_LOCALE:
            return changeLocale(state, action);
        default:
            return state;
    }
};

export default localeReducer;