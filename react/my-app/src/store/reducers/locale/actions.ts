import { ChangeLocaleAction } from "./locale-reducer-interface";
import { supportedLocales } from "../../../util/locales/supported-locales";

export const actionTypes = {
    CHANGE_LOCALE: "[LOCALE]CHANGE_LOCALE",
};

export const actionCreators = {
    changeLocale: (payload: typeof supportedLocales.BG | typeof supportedLocales.en_US): ChangeLocaleAction => {
        return {
            type: actionTypes.CHANGE_LOCALE,
            payload
        };
    },
};