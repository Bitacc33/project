import { LocaleReducer } from "./reducers/locale/locale-reducer-interface";
import { AuthReducer } from "./reducers/auth/interfaces";

interface SystemState {
    lcl: LocaleReducer,
    auth: AuthReducer,
}

export default SystemState;