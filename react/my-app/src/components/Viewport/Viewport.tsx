import React, { useEffect } from "react";

import Header from "../navigation/Header/Header";
import { Container } from "react-bootstrap";
import Routes from "../Routes/Routes";
import Footer from "../navigation/Footer/Footer";
import Spinner from "../shared/Spinner/Spinner";

import SystemState from "../../store/system-state";
import { actionCreators } from "../../store/reducers/auth/actions";

import { connect } from "react-redux";
import { LoggedUser } from "../../services/user-service/interfaces";

interface Props {
    locale: string,
    user: LoggedUser | null | undefined,
    firstTimeLoadHandler: any,
    isAuthenticating: boolean,
}

const ViewPort = ({ locale, user, firstTimeLoadHandler, isAuthenticating }: Props) => {
    const isLoggedIn = !!user;

    useEffect(() => {
        firstTimeLoadHandler();
    }, []);

    let content = (
        <div className="viewport">
            <Header locale={locale} isLoggedIn={isLoggedIn} user={user} />
            <Container>
                <Routes locale={locale} isLoggedIn={isLoggedIn} user={user} />
            </Container>
            <Footer locale={locale} />
        </div>
    );

    if (isAuthenticating) {
        content = (<Spinner />)
    }

    return content;
};

const mapStateToProps = (state: SystemState) => {
    return {
        locale: state.lcl.lang,
        user: state.auth.user,
        isAuthenticating: state.auth.isAuthenticating,
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        firstTimeLoadHandler: () => dispatch(actionCreators.firstTimeLoadAuth()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPort);