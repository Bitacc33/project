import React from "react";

import { configure, shallow, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Homepage from "./Homepage";
import GuestHomepage from "./GuestHomepage/GuestHomepage";
import AuthHomepage from "./AuthenticatedHomepage/AuthenticatedHomepage";
import AuthenticatedHomepage from "./AuthenticatedHomepage/AuthenticatedHomepage";

configure({ adapter: new Adapter() });

describe("<Homepage />", () => {
    let wrapper: ShallowWrapper;

    beforeEach(() => {
        wrapper = shallow(<Homepage />);
    })

    it("should display guest page when user is not authenticated", () => {
        wrapper.equals(<GuestHomepage />);
    });

    it("should display authenticated page when user is authenticated", () => {
        const username = "Ivan";
        wrapper.equals(<AuthenticatedHomepage username={username} />);
    });
})