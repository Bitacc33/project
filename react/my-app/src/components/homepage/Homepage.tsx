import React, { Fragment } from "react";

import GuestHomepage from "./GuestHomepage/GuestHomepage";
import AuthenticatedHomepage from "./AuthenticatedHomepage/AuthenticatedHomepage";

interface Props {
    username?: string,
}

const Homepage = ({ username }: Props) => {
    return <Fragment>
        {username
            ? <AuthenticatedHomepage username={username} />
            : <GuestHomepage />}
    </Fragment>
};

export default Homepage;