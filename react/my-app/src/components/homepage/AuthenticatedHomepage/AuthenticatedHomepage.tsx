import React from "react";

interface Props {
    username?: string,
}

const AuthenticatedHomepage = ({ username }: Props) => {
    return <div>
        <p>
            Hello {username}
        </p>
    </div>
};

export default AuthenticatedHomepage;