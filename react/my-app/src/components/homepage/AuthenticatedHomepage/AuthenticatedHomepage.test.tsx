import React from "react";

import Adapter from "enzyme-adapter-react-16";
import { configure, shallow, ShallowWrapper } from "enzyme";

import AuthenticatedHomepage from "./AuthenticatedHomepage";

configure({ adapter: new Adapter() });

describe("<AuthenticatedHomepage />", () => {
    let wrapper: ShallowWrapper;
    const username = "Random";
    
    beforeEach(() => {
        wrapper = shallow(<AuthenticatedHomepage username={username} />);
    });

    it("should display correct username", () => {
        wrapper.contains(<p>{username}</p>);
    });
})