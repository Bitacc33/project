import React, { useState, useEffect } from "react";

import productService from "../../../services/product-service/product-service";
import Product from "../product-interface";

import ProductCard from "../ProductCard/ProductCard";
import { CardColumns } from "react-bootstrap";

const BestSeller = () => {
    const [products, setProducts] = useState([] as Array<Product>);

    console.log(products);

    useEffect(() => {
        productService.getBestSellers()
            .then(({ data }) => {
                setProducts(data.data);
            })
    }, []);

    return <CardColumns>
        {products.length > 0 && products.map(product => {
            return <ProductCard key={product.id} product={product} />
        })}
    </CardColumns>
};

export default BestSeller;