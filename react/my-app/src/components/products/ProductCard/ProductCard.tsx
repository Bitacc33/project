import React from "react";
import "./ProductCard.css";

import Product from "../product-interface";
import { Card, Button } from "react-bootstrap";

interface Props {
    product: Product,
}

const ProductCard = ({ product }: Props) => {
    return <Card>
        <Card.Body>
            <Card.Title>{product.name}</Card.Title>
            <Card.Text>
                {product.long_description}
            </Card.Text>
            <Card.Text>
                <Button variant="outline-info">Details</Button>
            </Card.Text>
        </Card.Body>
    </Card>
};

export default ProductCard;