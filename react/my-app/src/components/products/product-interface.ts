interface Product {
    id: string,
    long_description: string,
    name: string,
}

export default Product;