import React, { Fragment, useMemo } from "react";
import "../Auth.css";

import { Formik, Field } from "formik";
import { Form, InputGroup, Button, Col } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

import schema from "./validation-schema";
import registrationDispenser from "../../../util/locales/dispensers/forms/registration/registration";

import { RegisterFormData } from "./register-customer-interface";

import userService from "../../../services/user-service/user-service";

interface Props {
    locale: string,
}

const Register: React.FC<Props> = ({ locale }) => {
    const history = useHistory();
    const translates = registrationDispenser(locale);

    const onbSubmit = (formData: any) => {
        const userData = {
            password: formData.password,
            customer: {
                email: formData.email,
                last_name: formData.last_name,
                login: formData.login
            }
        };

        userService.register(userData)
            .then(resp => {
                history.push("/login");
            })
            .catch(err => {
                console.log(err);
            })
    };

    return (
        <Formik
            validationSchema={schema}
            onSubmit={onbSubmit}
            initialValues={{
                email: "",
                login: "",
                password: "",
                last_name: "",
                confirmPassword: "",
            }}
        >
            {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                touched,
                isValid,
                errors,
            }) => (
                    <Fragment>
                        <Form onSubmit={handleSubmit}>
                            <h1 className="page-title">{translates.formHeader}</h1>
                            <Form.Group controlId="email">
                                <Form.Label>{translates.emailLabel}*</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>@</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="email"
                                        name="email"
                                        value={values.email}
                                        isInvalid={touched.email && !!errors.email}
                                        isValid={touched.email && !errors.email}
                                    >
                                    </Field>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.email}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="username">
                                <Form.Label>{translates.usernameLabel}*</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-user" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="text"
                                        name="login"
                                        value={values.login}
                                        isInvalid={touched.login && !!errors.login}
                                        isValid={touched.login && !errors.login}
                                    >
                                    </Field>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.login}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="lastName">
                                <Form.Label>{translates.last_nameLabel}*</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-user" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="text"
                                        name="last_name"
                                        value={values.last_name}
                                        isInvalid={touched.last_name && !!errors.last_name}
                                        isValid={touched.last_name && !errors.last_name}
                                    >
                                    </Field>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.last_name}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="password">
                                <Form.Label>{translates.passwordLabel}*</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-lock" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="password"
                                        name="password"
                                        value={values.password}
                                        isInvalid={touched.password && !!errors.password}
                                        isValid={touched.password && !errors.password}
                                    >
                                    </Field>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.password}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="confirmPasswrord">
                                <Form.Label>{translates.confirmPasswordLabel}*</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-lock" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="password"
                                        name="confirmPassword"
                                        value={values.confirmPassword}
                                        isInvalid={touched.confirmPassword && !!errors.confirmPassword}
                                        isValid={touched.confirmPassword && !errors.confirmPassword}
                                    >
                                    </Field>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.confirmPassword}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Col className="text-center">
                                <Button type="submit" variant="warning">{translates.submitBtn}</Button>
                            </Col>
                        </Form>
                        <div className="auth-help">
                            <Link to="/login">
                                {translates.hasAnAccount}
                            </Link>
                        </div>
                    </Fragment>
                )}
        </Formik>
    );
}

export default Register;