export interface RegisterFormData {
    password: string,
    email: string,
    login: string,
    last_name: string,
}

interface RegisterCustomer {
    password: string,
    customer: {
        login: string
        email: string,
        last_name: string,
    },
}

export default RegisterCustomer;