import React from "react";

import { configure, shallow, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import Login from "./Login";
import { Form } from "react-bootstrap";
import Spinner from "../../shared/Spinner/Spinner";

const mockStore = configureStore([]);
configure({ adapter: new Adapter() });

describe("<Login />", () => {
    const initialLoadingProps = { error: null, isLoading: false, success: false };
    let wrapper: ShallowWrapper;
    beforeEach(() => {
        const store = mockStore({})
        wrapper = shallow(<Provider store={store}><Login /></Provider>);
        wrapper.setProps(initialLoadingProps);
    });

    it("should display error", () => {
        const error = "Wrong username or pass";
        wrapper.setProps({ ...initialLoadingProps, error: error })
        expect(wrapper.contains(<Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>));
    });

    it("should not display error", () => {
        expect(wrapper.contains(<Form.Control.Feedback type="invalid"></Form.Control.Feedback>));
    });

    it("should display <Spinner /> while working on login req", () => {
        wrapper.setProps({ ...initialLoadingProps, isLoading: true });
        wrapper.equals(<Spinner />);
    });
});