import React, { Fragment } from "react";
import "../Auth.css";

import { Formik, Field } from "formik";
import { Link, useHistory } from "react-router-dom";
import { Form, Button, Col, FormGroup } from "react-bootstrap";
import { InputGroup } from "react-bootstrap";
import Spinner from "../../shared/Spinner/Spinner";

import { connect } from "react-redux";
import { Credentials } from "../../../services/user-service/interfaces";
import { actionCreators } from "../../../store/reducers/auth/actions";
import { loginSelector } from "../../../store/reducers/auth/selectors";

import SystemState from "../../../store/system-state";

interface Props {
    authHandler(credentials: Credentials): any,
    loginProps: {
        error: string | null | undefined,
        isLoading: boolean,
        success: boolean,
    }
}

const LogIn = ({ authHandler, loginProps }: Props) => {
    const history = useHistory();
    const { error, isLoading, success } = loginProps;

    const onSubmit = (credentials: Credentials) => {
        authHandler(credentials);
    }

    if (success) {
        history.push("/");
    }

    return (!isLoading ?
        <Formik
            onSubmit={onSubmit}
            initialValues={{
                email: '',
                password: "",
            }}
        >
            {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                touched,
            }) => (
                    <Fragment>
                        <Form noValidate onSubmit={handleSubmit}>
                            <h1 className="page-title">Log In</h1>
                            <Form.Group controlId="email-username">
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-user" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="text"
                                        name="email"
                                        placeholder="email..."
                                        value={values.email}
                                        isInvalid={!touched.email && !!error}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {error}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="password">
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text><i className="fas fa-lock" /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Field as={Form.Control}
                                        type="password"
                                        name="password"
                                        value={values.password}
                                        placeholder="password..."
                                    />
                                </InputGroup>
                            </Form.Group>
                            <Col className="text-center">
                                <Button type="submit" variant="warning"><i className="fas fa-sign-in-alt" /> Log In</Button>
                            </Col>
                        </Form>
                        <div className="auth-help">
                            <Link className="anchor" to="/register">
                                Create account
                            </Link>
                            <Link className="anchor" to="/account-recovery">
                                Trouble logging in?
                            </Link>
                        </div>
                    </Fragment>
                )}
        </Formik>
        : <Spinner />);
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        authHandler: (credentials: Credentials) => dispatch(actionCreators.startLogin(credentials)),
    }
};

const mapStateToProps = (state: SystemState) => {
    return {
        loginProps: loginSelector(state),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);