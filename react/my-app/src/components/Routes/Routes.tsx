import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";

import Register from "../auth/Register/Register";
import Login from "../auth/Login/Login";
import BestSellers from "../products/BestSellers/BestSellers";
import Homepage from "../homepage/Homepage";
import { LoggedUser } from "../../services/user-service/interfaces";

interface Props {
    user: LoggedUser,
    isLoggedIn: boolean,
    locale: string,
}

const Routes: React.FC<Props> = ({ locale, user }) => {
    return (
        <Fragment>
            <Switch>
                <Route path="/register" render={(props) => (
                    <Register {...props} locale={locale} />
                )} />
                <Route path="/login" component={Login} />
                <Route path="/best-sellers" component={BestSellers} />
                <Route path="/" render={(props) => (
                    <Homepage {...props} username={user && user.lastName} />
                )} />
            </Switch>
        </Fragment>
    );
};

export default Routes;
