import React from "react";
import "./Footer.css";

import { Link } from "react-router-dom";

import instagram from "../../../static/images/social/instagram.svg";
import facebook from "../../../static/images/social/facebook.svg";
import twitter from "../../../static/images/social/twitter.svg";

import socialLinks from "../../../util/constants/social";
import { footerDispenser } from "../../../util/locales/dispensers/navigation/navigation";

interface Props {
    locale: string,
}

const Footer: React.FC<Props> = ({ locale }) => {
    const translates = footerDispenser(locale);

    return (
        <footer className="site-footer">
            <div className="site-links">
                <Link className="anchor footer-link" to="/careers">
                    {translates.careers}
                </Link>
                <Link className="anchor footer-link" to="/about">
                    {translates.about}
                </Link>
                <Link className="anchor footer-link" to="/contacts">
                    {translates.contacts}
                </Link>
            </div>
            <div className="footer-legal">
                <div className="copyright">
                    <span>&copy;{translates.trademark}</span>
                </div>
                <div className="trademark">
                    <span>{translates.rights}</span>
                </div>
            </div>
            <div className="footer-social">
                <a href={socialLinks.instagram}>
                    <img className="social-icon" src={instagram} alt="instagram logo" />
                </a>
                <a href={socialLinks.facebook}>
                    <img className="social-icon" src={facebook} alt="facebook logo" />
                </a>
                <a href={socialLinks.twitter}>
                    <img className="social-icon" src={twitter} alt="twitter logo" />
                </a>
            </div>
        </footer>
    );
};

export default Footer;