import React, { Fragment } from "react";
import "./Header.css";

import { NavDropdown, Navbar, Nav } from "react-bootstrap";
import LocaleChanger from "./LocaleChanger/LocaleChanger";

import { headerDispenser } from "../../../util/locales/dispensers/navigation/navigation";
import { languageLiterals } from "../../../util/locales/supported-locales";

import { Link, useHistory } from "react-router-dom";
import { LoggedUser } from "../../../services/user-service/interfaces";

import { connect } from "react-redux";
import { actionCreators } from "../../../store/reducers/auth/actions";

interface Props {
    isLoggedIn: boolean,
    user: LoggedUser | null | undefined,
    locale: string,
    onLogout: any,
}

const Navigation: React.FC<Props> = ({ isLoggedIn, user, locale, onLogout }) => {
    const headerTranslates = headerDispenser(locale);

    const history = useHistory();

    const logoutHandler = async () => {
        await onLogout();
        history.push("/");
    };

    return (
        <Fragment>
            <header className="site-header">
                <Navbar collapseOnSelect expand="lg" className="navbar navbar-dark navbar-expand-md justify-content-center navbar-bg-dark">
                    <Navbar.Brand className="d-flex w-50 mr-auto">
                        <Link to="/" className="navbar-brand">
                            {/* <img className="logo-icon" src={} alt="brand-logo" /> */}
                            <span className="brand-name">E-Shop</span>
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="w-100">
                        <Nav className="navbar-nav w-100 justify-content-center">
                            <NavDropdown title={languageLiterals[locale]} id="trips-dropdown">
                                <LocaleChanger />
                            </NavDropdown>
                            <Link to="/about" className="nav-link">{headerTranslates.about}</Link>
                            <Link to="/best-sellers" className="nav-link">{headerTranslates.bestSellers}</Link>
                        </Nav>
                        <Nav className="nav navbar-nav ml-auto w-100 justify-content-end">
                            {isLoggedIn
                                ? (<Fragment>
                                    <NavDropdown alignRight title={user && user.lastName} id="account-controll-drpdown">
                                        <NavDropdown.Item as={Link} to="/user/profile"><i className="fas fa-cog" /> Profile</NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item as={Link} onClick={logoutHandler} to="/logout"><i className="fas fa-sign-out-alt" /> Logout</NavDropdown.Item>
                                    </NavDropdown>
                                </Fragment>)
                                : (<Fragment><Link to="/login" className="nav-link auth login">{headerTranslates.loginBtn}</Link>
                                    <Link to="/register" className="nav-link auth sign-up">{headerTranslates.signUpBtn}</Link></Fragment>)
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </header>
        </Fragment>
    )
};

const mapDispatchToState = (dispatch: any) => {
    return {
        onLogout: () => dispatch(actionCreators.logout()),
    };
};

export default connect(null, mapDispatchToState)(Navigation);