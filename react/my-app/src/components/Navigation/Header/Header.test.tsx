import React, { Fragment } from "react";

import { Link } from "react-router-dom";

import { configure, shallow, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { NavDropdown } from "react-bootstrap";
import Header from "./Header";
import { LoggedUser } from "../../../services/user-service/interfaces";

const mockStore = configureStore([]);
configure({ adapter: new Adapter() });

describe("<Header />", () => {
    let wrapper: ShallowWrapper;
    
    beforeEach(() => {
        const store = mockStore({});
        wrapper = shallow(<Provider store={store}><Header isLoggedIn={false} locale={"en_US"} user={null} /></Provider>);
    });

    it("should display log in and register buttons if user is not authenticated", () => {
        expect(wrapper.contains(
            <Fragment>
                <Link to="/login" className="nav-link auth login">Log in</Link>
                <Link to="/register" className="nav-link auth sign-up">Sign up</Link>
            </Fragment>
        ));
    });


    it("should profile dorpdown menu when user is authenticated", () => {
        const user: LoggedUser = { lastName: "pesho", userId: "qwe" };
        wrapper.setProps({ isLoggedIn: true, user: user });

        expect(wrapper.contains(
            <Fragment>
                <NavDropdown alignRight title={user && user.lastName} id="account-controll-drpdown">
                    <NavDropdown.Item as={Link} to="/user/profile"><i className="fas fa-cog" /> Profile</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} to="/logout"><i className="fas fa-sign-out-alt" /> Logout</NavDropdown.Item>
                </NavDropdown>
            </Fragment>
        ));
    });


});

