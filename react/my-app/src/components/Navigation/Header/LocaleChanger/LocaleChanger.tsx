import React, { Fragment } from "react";

import { languageLiterals } from "../../../../util/locales/supported-locales";
import { actionCreators } from "../../../../store/reducers/locale/actions";
import { connect } from "react-redux";

import { NavDropdown } from "react-bootstrap";

interface Props {
    changeLocaleHandler: any
}

const LocaleChanger: React.FC<Props> = ({ changeLocaleHandler }) => {
    return (
        <Fragment>
            {Object.keys(languageLiterals).map(language => {
                return <NavDropdown.Item key={language} onClick={() => changeLocaleHandler(language)}>{languageLiterals[language]}</NavDropdown.Item>
            })}
        </Fragment>
    )
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        changeLocaleHandler: (locale: string) => dispatch(actionCreators.changeLocale(locale))
    };
};

export default connect(undefined, mapDispatchToProps)(LocaleChanger);