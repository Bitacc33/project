import React from "react";

import { configure, shallow, mount, ShallowWrapper } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import LocaleChanger from "./LocaleChanger";
import { NavDropdown } from "react-bootstrap";

import { languageLiterals } from "../../../../util/locales/supported-locales";

configure({ adapter: new Adapter() });
const mockStore = configureStore([]);

describe("<LocaleChanger />", () => {
    let wrapper: any;
    const store = mockStore({});

    beforeEach(() => {
        wrapper = shallow(<Provider store={store}><LocaleChanger /></Provider>);
    });

    it.only("should display all available locale options", () => {
        const localeOptionsCount = Object.keys(languageLiterals).length;
        wrapper = mount(<Provider store={store}><LocaleChanger /></Provider>);

        expect(wrapper.find(NavDropdown.Item)).toHaveLength(localeOptionsCount);
    })
});