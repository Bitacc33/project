export interface Credentials {
    email: string,
    password: string,
}

export interface LoggedUser {
    userId: string,
    lastName: string,
}