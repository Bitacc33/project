import initRequester from "../../util/requester/requester";
import { AxiosResponse } from "axios";

import { Credentials } from "./interfaces";
import RegisterCustomer from "../../components/auth/Register/register-customer-interface";

const BASE_URL = "http://localhost:8000/users";
const requester = initRequester(BASE_URL);

const END_POINTS = {
    login: "/login",
    authenticate: "/auth",
    logout: "/logout",
    register: "/register",
};

const userService = {
    login: (credentials: Credentials): Promise<AxiosResponse> => {
        return requester.post(END_POINTS.login, credentials);
    },

    authenticate: (): Promise<AxiosResponse> => {
        return requester.get(END_POINTS.authenticate);
    },

    logout: (): Promise<AxiosResponse> => {
        return requester.delete(END_POINTS.logout);
    },

    register: (userData: RegisterCustomer): Promise<AxiosResponse> => {
        return requester.post(END_POINTS.register, userData);
    },

};

export default userService;
