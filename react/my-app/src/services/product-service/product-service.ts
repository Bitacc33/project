import initRequester from "../../util/requester/requester";

const BASE_URL = "http://localhost:8000/products";
const requester = initRequester(BASE_URL);

const END_POINTS = {
    getBestSellers: "/best-sellers",
};

const productService = {
    getBestSellers: () => {
        return requester.get(END_POINTS.getBestSellers);
    }
};

export default productService;