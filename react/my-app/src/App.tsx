import React from 'react';
import './App.css';

import { BrowserRouter } from "react-router-dom";
import Viewport from "./components/Viewport/Viewport";

import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import localeReducer from "./store/reducers/locale/reducer";
import authReducer from "./store/reducers/auth/reducer";

const rootReducer = combineReducers({
  lcl: localeReducer,
  auth: authReducer,
});

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

// const store = createStore(rootReducer,
//   (window as any).__REDUX_DEVTOOLS_EXTENSION__
//   && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
// );

function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Viewport />
      </BrowserRouter>
    </Provider>
  );
}

export default App;