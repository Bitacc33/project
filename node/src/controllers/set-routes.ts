import { Application } from "express";

import userController from "./user-controller";
import productController from "./product-controller";


const setRoutes = (app: Application) => {
    app.use(userController);
    app.use(productController);
};

export default setRoutes;