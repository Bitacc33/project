import express from "express";
import { AxiosResponse, AxiosError } from "axios";

import productService from "../services/product-service/product-service";

const router = express.Router();

router.get("/products/best-sellers", (req, res) => {
    productService.getBestSelliners()
        .then(({ data }) => {
            res.status(200)
                .send(data);
        })
        .catch(err => {
            res.status(400)
                .send();
        });
});

export default router;

