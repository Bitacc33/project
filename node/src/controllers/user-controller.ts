import express, { Response } from "express";
import { AxiosResponse, AxiosError } from "axios";

import userService from "../services/user-service/user-service";
import Credentials, { LoggedUser } from "../services/user-service/credentials-interface";

import { COOKIE_NAME as AUTH_COOKIE_NAME } from "../util/constants/auth/auth";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/users/register", (req, res) => {
    userService.register(req.body)
        .then(resp => {
            res.status(201).send();
        })
        .catch((err: AxiosError) => {
            console.log(err.response);
            res.status(400).send();
        });
});

router.post("/users/login", (req, res) => {
    const credentials: Credentials = req.body;

    if (!credentials.email || !credentials.password) {
        res.status(400).send({ error: "Empty credentials." });

        return;
    }

    userService.login(req.body)
        .then((response) => {
            const userData = authSuccess(response, res);

            res.status(200)
                .send(userData);
        })
        .catch(err => {
            res.status(400)
                .send({ error: "Wrong username or password" });
        });
});

router.get("/users/auth", (req, res) => {
    const token = req.cookies[AUTH_COOKIE_NAME];

    if (!token) {
        res.status(400)
            .send("Token not present.");

        return;
    }

    userService.authenticate(token)
        .then(response => {
            const userData = authSuccess(response, res);
            res.status(200)
                .send(userData);
        }).catch(err => {
            res.status(400)
                .send();
        });
});

const authSuccess = (response: AxiosResponse, res: Response<any>): LoggedUser => {
    const authHeader: string = response.headers.authorization;
    const token = authHeader.replace("Bearer ", "");
    const tokenVals: any = jwt.decode(token);

    res.cookie(AUTH_COOKIE_NAME, token, { maxAge: tokenVals.exp, httpOnly: true, secure: false, });


    const { data } = response;
    const { customer_id: userId, last_name: lastName } = data;

    return ({ userId, lastName });
};

router.delete("/users/logout", (req, res) => {
    const token = req.cookies[AUTH_COOKIE_NAME];
    res.cookie(AUTH_COOKIE_NAME, "", { maxAge: 0, httpOnly: true });

    res.status(204)
        .send();

    //OCAPI throws ClientAccessForbiddenException for whatever reason
    //documentation unclear
    //unable to invalidate jwt at this point

    // userService.logout(token)
    //     .then(data => {
    //         res.cookie(AUTH_COOKIE_NAME, "", { maxAge: 0, httpOnly: true });

    //         res.status(204)
    //             .send();
    //     })
    //     .catch((err: AxiosError) => {
    //         console.log(err.response);
    //         res.status(400)
    //             .send();
    //     });
});

export default router;