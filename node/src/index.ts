import express from "express";

import cookieParser from "cookie-parser";
const cors = require("cors");

import setRoutes from "./controllers/set-routes";
import appConstants from "./util/constants/app-config";

const app = express();

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,
};

app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());

setRoutes(app);

app.listen(appConstants.PORT, () => {
    console.log(`App is running on port: ${appConstants.PORT}`);
});