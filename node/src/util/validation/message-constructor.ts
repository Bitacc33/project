export const required = (param: string): string => {
    return `${param} is required`
};

export const maxLength = (param: string, length: number): string => {
    return `${param} cannot be more than ${length} characters`;
};

export const minLength = (param: string, length: number): string => {
    return `${param} must be at least ${length} characters`;
};

export const invalid = (param: string): string => {
    return `Invalid ${param}`;
}