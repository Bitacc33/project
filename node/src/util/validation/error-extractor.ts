import * as yup from "yup";

interface ValidationError {
    field: string,
    message: string,
}

export const extractErrors = (schema: yup.ObjectSchema, data: any): Promise<Array<ValidationError>> => {
    return schema.validate(data, { abortEarly: false })
        .then(({ }: any) => [])
        .catch((data: any) => {
            return data.inner.map((error: yup.ValidationError) => {
                return {
                    field: getParamName(error.path),
                    message: error.message
                };
            });
        })
};

const getParamName = (path: string) => {
    //if path contains "." param name starts from the last index of the dot + 1
    //otherwise param name starts fro index 0

    let dotIndex = path.lastIndexOf(".");
    dotIndex = dotIndex > 0
        ? dotIndex + 1
        : 0;
    return path.substring(dotIndex);
};