import axios, { AxiosRequestConfig, AxiosInstance } from "axios";

import Requester, { AuthorizationOptions } from "./requester-interface";

const buildRequestConfig = (method: "GET" | "POST" | "PATCH" | "DELETE", endpoint: string, data?: any, authOptions?: AuthorizationOptions): AxiosRequestConfig => {
    const headers: any = {
        "Content-Type": "application/json",
        credentials: 'include',
        "x-dw-client-id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    };

    if (authOptions) {
        const authHeader = `${authOptions.type} ${authOptions.type === "Basic"
            ? Buffer.from(`${authOptions.payload.username}:${authOptions.payload.password}`, "utf-8").toString("base64")
            : `${authOptions.payload.token}`}`;

        headers.Authorization = authHeader;
    }

    return { headers, method, url: endpoint, data };
};

const prepareRequest = (method: "GET" | "POST" | "PATCH" | "DELETE", axios: AxiosInstance) => {
    return (endpoint: string, data?: any, authOptions?: AuthorizationOptions) => {
        const requestConfig = buildRequestConfig(method, endpoint, data, authOptions);

        return axios.request(requestConfig);
    }
}

const initRequester = (baseURL: string): Requester => {
    const instance = axios.create({ baseURL });

    return {
        get: prepareRequest("GET", instance),
        post: prepareRequest("POST", instance),
        patch: prepareRequest("PATCH", instance),
        delete: prepareRequest("DELETE", instance),
    };
};

export default initRequester;