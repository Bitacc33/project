interface Requester {
    get(endpoint: string): Promise<any>,
    post(endpoint: string, data?: any, authOptions?: AuthorizationOptions): Promise<any>,
    delete(endpoint: string, data?: any, authOptions?: AuthorizationOptions): Promise<any>,
    patch(endpoint: string, data?: any, authOptions?: AuthorizationOptions): Promise<any>,
}

export interface BasicAuth {
    type: "Basic",
    payload: {
        username: string,
        password: string,
    }
}

export interface BearerAuth {
    type: "Bearer",
    payload: {
        token: string,
    }
}

export type AuthorizationOptions = BasicAuth | BearerAuth;
export default Requester;