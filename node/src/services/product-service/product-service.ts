import initRequester from "../../util/requester/requester";

const BASE_URL = "https://dev20-web-ecommera.demandware.net/s/SiteGenesis/dw/shop/v20_4/products";
const fetch = initRequester(BASE_URL);

const END_POINTS = {
    getBestSellers: "/(008884303989,008885538441,013742002805,013742335514,029407331258,095068015585,25493689)",
};

const productService = {
    getBestSelliners: () => {
        return fetch.get(END_POINTS.getBestSellers);
    }
};


export default productService;