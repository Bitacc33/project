interface RegisterCustomer {
    password: string,
    customer: {
        login: string
        email: string,
        last_name: string,
    },
}

export default RegisterCustomer;