interface Credentials {
    email: string,
    password: string,
}

export interface LoggedUser {
    userId: string,
    lastName: string,
}

export interface AuthBody {
    type: "credentials" | "refresh" | "guest"
}

export default Credentials;