import * as yup from "yup";
import { minLength, maxLength, required, invalid } from "../../util/validation/message-constructor";

const LASTNAME_MIN_LENGTH = 3;
const LASTNAME_MAX_LENGTH = 256;

const PASSWORD_MIN_LENGTH = 6;
const PASSWORD_MAX_LENGTH = 4096;

const LOGIN_MIN_LENGTH = 5;
const LOGIN_MAX_LENGTH = 256;

const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
const EMAIL_MAX_LENGTH = 256;
const EMAIL_MIN = 5;

const schema = yup.object({
    password: yup.string()
        .required(required("Password"))
        .min(PASSWORD_MIN_LENGTH, minLength("Password", PASSWORD_MIN_LENGTH))
        .max(PASSWORD_MAX_LENGTH, maxLength("Password", PASSWORD_MAX_LENGTH)),
    customer: yup.object().shape({
        login: yup.string()
            .required(required("Login"))
            .min(LOGIN_MIN_LENGTH, minLength("Login", LOGIN_MIN_LENGTH))
            .max(LOGIN_MAX_LENGTH, maxLength("Login", LOGIN_MAX_LENGTH)),
        "last_name": yup.string()
            .required(required("Last Name"))
            .min(LASTNAME_MIN_LENGTH, minLength("Last Name", LASTNAME_MIN_LENGTH))
            .max(LASTNAME_MAX_LENGTH, maxLength("Last name", LASTNAME_MAX_LENGTH)),
        email: yup.string()
            .required(required("Email"))
            .min(EMAIL_MIN, minLength("EMAIL", EMAIL_MIN))
            .max(EMAIL_MAX_LENGTH, maxLength("Email", EMAIL_MAX_LENGTH))
            .matches(EMAIL_REGEX, invalid("Email"))
    })
});

export default schema;