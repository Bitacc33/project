import initRequester from "../../util/requester/requester";
import { extractErrors } from "../../util/validation/error-extractor";

import RegisterCustomer from "./register-customer-interface";
import registerValidationSchema from "./register-validation-schema";

import Credentials, { LoggedUser, AuthBody } from "./credentials-interface";
import { BasicAuth, BearerAuth } from "../../util/requester/requester-interface";
import { AxiosPromise, AxiosResponse } from "axios";

const BASE_URL = "https://dev20-web-ecommera.demandware.net/s/SiteGenesis/dw/shop/v20_4/customers";
const fetch = initRequester(BASE_URL);

const END_POINTS = {
    register: "",
    login: "/auth",
    authenticate: "/auth",
    logout: "/auth",
    guestToken: "/auth"
};

const userService = {
    register: async (userData: RegisterCustomer): Promise<AxiosResponse> => {
        const errors = await extractErrors(registerValidationSchema, userData);

        if (errors.length > 0) {
            throw new Error(JSON.stringify(errors));
        }

        const body: AuthBody = {
            type: "guest"
        };

        const response: AxiosResponse = await fetch.post(END_POINTS.guestToken, body);
        const guestToken = (response.headers.authorization as string).replace("Bearer ", "");

        const authOptions: BearerAuth = {
            type: "Bearer",
            payload: {
                token: guestToken
            },
        };

        return fetch.post(END_POINTS.register, userData, authOptions);
    },

    login: (credentials: Credentials): Promise<AxiosResponse> => {
        const authOptions: BasicAuth = {
            type: "Basic",
            payload: {
                username: credentials.email,
                password: credentials.password,
            },
        };

        const body: AuthBody = {
            type: "credentials"
        };

        return fetch.post(END_POINTS.login, body, authOptions);
    },

    authenticate: (token: string): Promise<AxiosResponse> => {
        const authOptions: BearerAuth = {
            type: "Bearer",
            payload: {
                token
            }
        };

        const body: AuthBody = {
            type: "refresh"
        };

        return fetch.post(END_POINTS.authenticate, body, authOptions);
    },

    logout: (token: string): Promise<AxiosResponse> => {
        const authOptions: BearerAuth = {
            type: "Bearer",
            payload: {
                token
            }
        };

        return fetch.delete(END_POINTS.logout, null, authOptions);
    },
}

export default userService;